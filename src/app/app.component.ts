import { Component, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  COLOR_PALETTE = ['#A7226E', '#EC2049', '#F26B38', '#F7DB4F', '#2F9599'];
  NUM_OF_CIRCLES = 20;

  entered = false;
  bounceDroplet = false;
  animateRipple = false;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly renderer: Renderer2
  ) {}

  onDropletClick(): void {
    this.bounceDroplet = true;
    this.animateRipple = true;

    setTimeout(() => {
      this.bounceDroplet = false;
      this.createRipple();
      setTimeout(() => {
        this.animateRipple = false;
      }, this.NUM_OF_CIRCLES * 120);
    }, 800);
  }

  createRipple(): void {
    for (let i = 0; i <= this.NUM_OF_CIRCLES; i++) {
      const addAfter = 100 * i;

      console.log(i);
      if (i === this.NUM_OF_CIRCLES) {
        setTimeout(() => {
          this.drawCircle(i, '#fff');
        }, addAfter);
      } else {
        setTimeout(() => {
          const colorIdx = i % this.COLOR_PALETTE.length;
          const color = this.COLOR_PALETTE[colorIdx];
          this.drawCircle(i, color);
        }, addAfter);
      }
    }
  }

  drawCircle(removeAfter: number, color: string): void {
    const circle = this.renderer.createElement('section');
    this.renderer.addClass(circle, 'fill');
    this.renderer.addClass(circle, 'fill-center');
    this.renderer.setStyle(circle, 'background-color', color);
    this.renderer.appendChild(this.document.body, circle);
    setTimeout(() => {
      this.renderer.removeChild(this.document.body, circle);
    }, removeAfter * 700);
  }

  onMouseEnter(): void {
    this.entered = true;
  }

  onMouseLeave(): void {
    this.entered = false;
  }

  getRandomHexColor(): string {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);

    const rHex = this.decimalToHex(r);
    const gHex = this.decimalToHex(g);
    const bHex = this.decimalToHex(b);

    return `#${rHex}${gHex}${bHex}`;
  }

  decimalToHex(decimal: number): string {
    return (decimal + Math.pow(16, 6)).toString(16).substr(5);
  }
}
